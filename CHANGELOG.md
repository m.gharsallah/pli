# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

-   Added better logging
-   Added a validation check for the config file
-   Added support for custom paths per language

### Fixed

-   Fixed error where missing locales breaks the push commands
-   Fixed error which happens when saving locale files to non-existing directories

## [0.2.0] - 2019-11-23

### Added

-   Added Changelog
-   Added debug logs
-   Added support for overwriting terms

## [0.0.0] - 2019-11-23

### Added

-   Added pli api and cli commands
