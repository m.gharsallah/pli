#!/usr/bin/env node
const version = require('../package.json').version;
const prog = require('caporal');

const api = require('../src');
const logger = require('../src/logger');

prog.version(version)
    .logger(logger)
    .command('pull', 'Pull latest translation terms')
    .action(function() {
        api.pull();
    })
    .command('push', 'Push new translation terms')
    .option('-o, --overwrite', 'Overwrite existing terms. Pull first!')
    .action(function(args, options) {
        api.push(options);
    });

prog.parse(process.argv);
