# PLI a simple Poeditor CLI

A usefull cli tool for fetching and adding terms from [Poeditor.com](https://poeditor.com)

## Installation

```sh
npm i -g pli
```

## Configuration

Create a `pli.yaml` configuration file in the root folder of your app and add the following properties:

Supported file formats: **pli.yaml**, **pli.yml** and **pli.json**

```yaml
api_token: 321452345643564536453645 # Your poeditor api token
project_id: 4886 # Your project id
type: apple_strings # The File format: https://poeditor.com/help/#SupportedFormats
tags: [ios] # Optional tags list. Will be assigned to new terms

# At least one (path or paths) parameter should be provided
path: App/Common/Assets/Strings/{LANGUAGE}.lproj/Localizable.strings
# Path of locale files in your project. Use {LANGUAGE} as placeholder of the language code.
paths:
    en: App/Common/Assets/Strings/Base.lproj/Localizable.strings
# Custom paths per languge. Important! custom paths cannot be be pushed
```

## Usage

Fetch the translation from poeditor using:

```sh
$ pli pull
```

This command will load all available languages and store them using the path format defined in the configuration

---

Add new the translation terms to poeditor using:

```sh
$ pli push [-o, --overwrite]
```

This command will only add new terms. It won't update changed terms to prevent overwriting them unless you use the `-o` flag
**Important**: Be careful with using the overwrite flag it will it will updating the terms even if they have a recent version on POEditor
