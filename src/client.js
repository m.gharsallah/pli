const fetch = require('node-fetch');
const FormData = require('form-data');
const fs = require('fs');

const endpoint = require('./constants').endpoint;

async function getLanguages(api_token, id) {
    const params = new URLSearchParams({ api_token, id });

    const response = await fetch(endpoint + '/languages/list', {
        method: 'post',
        body: params,
    });
    const jsonResponse = await response.json();

    return jsonResponse.result.languages;
}

async function getFileURL(api_token, id, type, language) {
    const params = new URLSearchParams({ api_token, id, language, type });

    const response = await fetch(endpoint + '/projects/export', {
        method: 'post',
        body: params,
    });
    const jsonResponse = await response.json();

    return jsonResponse.result.url;
}

async function downloadFile(url) {
    const response = await fetch(url);
    const body = await response.buffer();

    return body;
}

async function uploadFile(api_token, id, language, filePath, tags, overwrite) {
    const form = new FormData();
    form.append('api_token', api_token);
    form.append('id', id);
    form.append('language', language);
    form.append('file', fs.createReadStream(filePath));
    form.append('tags', tags.join(','));
    form.append('updating', 'terms_translations');
    form.append('overwrite', overwrite ? 1 : 0);
    form.append('fuzzy_trigger', 1);

    const response = await fetch(endpoint + '/projects/upload', {
        method: 'post',
        body: form,
    });
    const jsonResponse = await response.json();

    return jsonResponse.result;
}

module.exports = {
    getLanguages,
    getFileURL,
    downloadFile,
    uploadFile,
};
