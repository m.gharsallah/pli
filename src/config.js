const cosmiconfig = require('cosmiconfig');

const moduleName = require('./constants').moduleName;
const logger = require('./logger');

function getConfiguration() {
    logger.debug('looking for configuration...');
    const explorer = cosmiconfig(moduleName, {
        searchPlaces: [`${moduleName}.yaml`, `${moduleName}.yml`, `${moduleName}.json`],
    });
    const result = explorer.searchSync();
    if (result === null || result.isEmpty === true) {
        logger.error('Invalid or Missing configuration file');
        process.exit(1);
    }
    logger.debug('Configuration successfully found...', result);

    validateConfiguration(result.config);

    return result.config;
}

function validateConfiguration(configuration) {
    if (!configuration.api_token) {
        logger.error('Invalid configuration: api_token is missing');
        process.exit(1);
    }
    if (!configuration.project_id) {
        logger.error('Invalid configuration: project_id is missing');
        process.exit(1);
    }
    if (!configuration.type) {
        logger.error('Invalid configuration: type is missing');
        process.exit(1);
    }
    if (!configuration.path || !configuration.paths) {
        logger.error('Invalid configuration: No path/paths configuration was defined');
        process.exit(1);
    }
}

module.exports = {
    getConfiguration,
};
