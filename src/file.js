const fs = require('fs-extra');

function getContent(path) {
    return fs.readFileSync(path, { encoding: 'utf8' });
}

function localExists(path) {
    return fs.pathExistsSync(path);
}

function saveContent(path, content) {
    fs.outputFileSync(path, content);
}

module.exports = {
    getContent,
    localExists,
    saveContent,
};
