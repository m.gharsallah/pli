const client = require('./client');
const config = require('./config');
const file = require('./file');
const logger = require('./logger');

async function pull() {
    const configuration = config.getConfiguration();

    logger.debug('loading project languages...');
    const languages = await client.getLanguages(configuration.api_token, configuration.project_id);
    logger.debug('Project languages successfully loaded', languages);

    logger.info(`🔎  ${languages.length} languages were found`);

    languages.forEach(async function(language) {
        logger.debug(`Loading ${language} file URL...`);
        const url = await client.getFileURL(
            configuration.api_token,
            configuration.project_id,
            configuration.type,
            language.code
        );
        logger.debug(`URL successfully loaded`, url);

        logger.debug(`Loading file...`);
        const content = await client.downloadFile(url);
        logger.debug(`File successfully loaded...`, ' \n----', content.toString(), '----');

        if (configuration.path) {
            const filePath = configuration.path.replace('{LANGUAGE}', language.code);
            logger.debug(`File will be saved to: `, filePath);
            file.saveContent(filePath, content);
            logger.info(`✅  ${filePath}`);
        }

        if (configuration.paths && configuration.paths[language.code]) {
            const filePath = configuration.paths[language.code];
            logger.debug(`File will be saved to: `, filePath);
            file.saveContent(filePath, content);
            logger.info(`✅  ${filePath}`);
        }
    });
}

async function push(options) {
    const configuration = config.getConfiguration();

    logger.debug('loading project languages...');
    const languages = await client.getLanguages(configuration.api_token, configuration.project_id);
    logger.debug('Project languages successfully loaded', languages);

    logger.info(`🔎  Updating languages...`);
    languages.forEach(async function(language) {
        const filePath = configuration.path.replace('{LANGUAGE}', language.code);

        if (!file.localExists(filePath)) {
            logger.debug(`locale for ${language.language} was not found in: ${filePath}`);
            return;
        }

        logger.debug(`Uploading file: ${filePath}`);
        const result = await client.uploadFile(
            configuration.api_token,
            configuration.project_id,
            language.code,
            filePath,
            configuration.tags,
            options.overwrite
        );

        logger.debug('File successfully uploaded', result);

        const updateLog = result.translations;
        logger.info(
            `   - ${language.name}: [ parsed: ${updateLog.parsed}, added: ${updateLog.added}, updated: ${updateLog.updated} ]`
        );
    });
}

module.exports = {
    pull,
    push,
};
